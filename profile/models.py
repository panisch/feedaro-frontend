from django.contrib.auth.models import User
from django.db import models

class UserProfile(models.Model):
    user = models.OneToOneField(User)

    # Other fields here
    #top_topics = models.CommaSeparatedIntegerField()
    accepted_eula = models.BooleanField()
    favorite_animal = models.CharField(max_length=20, default="Dragons.")
    
    def add_post(self, post_hash):
        pass
    
    def get_top_topics(self):
        pass
    
    
    
    
#class UserReadings(models.Model):
    #user = models.OneToOneField(User)
    
    #post_id = models.IntegerField()
    #reading_time =
    