from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns("",
    url(r"", include("posts.urls")),
    url(r'^stats/(?P<post_seqno>\d+)//(?P<link>\S*)/$', 'posts.views.external_link', name='stats'),
    url(r"^admin/", include(admin.site.urls)),

    url(r"^account/", include("account.urls")),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
