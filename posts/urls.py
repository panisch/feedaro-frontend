from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^$', 'posts.views.show_posts', name='home'),
    url(r'^(?P<page>\-?\d+)/$', 'posts.views.show_posts', name='home_no'),
    #url(r'^add/$', 'feed.views.add_feed', name='add_feed'),
    #url(r'^news/$', 'feed.views.show_news', name='show_news'),
    #url(r'^add_preview/$', 'feed.views.add_feed_preview', name='add_preview'),
    #url(r'^test/$', FeedFormPreview(FeedForm)),
)