from django.shortcuts import render_to_response, render
from django.template import RequestContext
import os, sys, inspect
import sys
import webbrowser
import logging
from logging.config import dictConfig
from django.conf import Settings
sys.path.append( '../feedaro_backend/feedaro-py' )
from feedaro.db import db_tools
from feedaro.stats import stats_tools
#from feedaro.lda import top_topics
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

logger = logging.getLogger('')

def show_posts(request,page=0):
    logger.debug(' Showing news')
    print page
    sim_posts = []
    posts = []
    cluster = []
    page_size=100
    if request.user.is_authenticated():
        posts = db_tools.get_posts().find().sort("date",-1).skip(int(page)*page_size).limit(page_size)
        #get posts from user similarity
        user_ids = [user.id for user in User.objects.all()]
        sim_user = stats_tools.get_similar_users(request.user.id, user_ids)
        if sim_user:
            sim_posts_seqnos = stats_tools.get_similar_posts_seqno(request.user.id,sim_user)
            if sim_posts_seqnos:
                sim_posts = db_tools.get_posts_by_seqno(sim_posts_seqnos)
        #get clustered posts
       # b = top_topics.get_top_topic()
        #print b[0]
        
    return render(request, "homepage.html", {"page":page, "posts":posts, "sim_posts":sim_posts})

def external_link(request, post_seqno="bad",link="bad"):
    """
    Redirects links and keeps track of them
    """
    if link != "bad":
        logger.debug("opening link: " )
        webbrowser.open_new_tab(link) 
    
    stats_tools.add_post_user(request.user.id, post_seqno)
    
    return HttpResponseRedirect(reverse('posts.views.show_posts'))

